// Mundo.cpp: implementation of the CMundo class.
 //
 //////////////////////////////////////////////////////////////////////
 
 #include "MundoCliente.h"
 
 #include <stdlib.h>
 #include <stdio.h>
 #include <string.h>
 #include <math.h>
 #include <unistd.h>
 #include <sys/types.h>
 #include <sys/stat.h>
 #include <sys/file.h>
 #include <sys/mman.h>
 #include <fcntl.h>
 #include <fstream>
 
 #ifdef __APPLE__
 #include <OpenGL/gl.h>
 #include <Glut/glut.h>
 #elif defined(_WIN32) || defined(_WIN64)
 #include <GLUT/glcmolorut.h>
 #else
 #include <GL/glut.h>
 #endif
 

#define MAX 200
 //////////////////////////////////////////////////////////////////////
 // Construction/Destruction
 //////////////////////////////////////////////////////////////////////
 
 CMundoCliente::CMundoCliente()
 {
     Init();
 }
 
 CMundoCliente::~CMundoCliente()
 {


    printf("He entrado al destructor del MundoCliente\n");
    
    //Para los fifos
    char mensajeroFin[MAX];
    sprintf(mensajeroFin,"-----Fin del juego-----\n");
    write(fd_fifoCoord,mensajeroFin,strlen(mensajeroFin)+1);
     close(fd_fifoCoord);//Cerrar fifo que recibir Coordenadas
    unlink("/tmp/FifoCoordenadas");//Eliminar FifoCoord
    write(fd_fifoCoord,mensajeroFin,strlen(mensajeroFin)+1);
    close(fd_fifoTeclas);//Cerrar fifo que enviar Teclas
    unlink("/tmp/FifoTecla");
    
     //Bot
     pdatosBot->accion=3;    //indicar a bot que ha acabado el juego
     munmap(pdatosBot,sizeof(datosBot));    //Cerrar la proyeccion
    
    
    unlink("/tmp/MemCompartida");
     
 }
 
 void CMundoCliente::InitGL()
 {
     //Habilitamos las luces, la renderizacion y el color de los materiales
     glEnable(GL_LIGHT0);
     glEnable(GL_LIGHTING);
     glEnable(GL_DEPTH_TEST);
     glEnable(GL_COLOR_MATERIAL);
     
     glMatrixMode(GL_PROJECTION);
     gluPerspective( 40.0, 800/600.0f, 0.1, 150);
 }
 
 void print(char *mensaje, int x, int y, float r, float g, float b)
 {
     glDisable (GL_LIGHTING);
     
     glMatrixMode(GL_TEXTURE);
     glPushMatrix();
     glLoadIdentity();
     
     glMatrixMode(GL_PROJECTION);
     glPushMatrix();
     glLoadIdentity();
     gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );
     
     glMatrixMode(GL_MODELVIEW);
     glPushMatrix();
     glLoadIdentity();
     
     glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
     glDisable(GL_DEPTH_TEST);
     glDisable(GL_BLEND);
     glColor3f(r,g,b);
     glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
     int len = strlen (mensaje );
     for (int i = 0; i < len; i++)
         glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
     
     glMatrixMode(GL_TEXTURE);
     glPopMatrix();
     
     glMatrixMode(GL_PROJECTION);
     glPopMatrix();
     
     glMatrixMode(GL_MODELVIEW);
     glPopMatrix();
     
     glEnable( GL_DEPTH_TEST );
 }
 void CMundoCliente::OnDraw()
 {
     //Borrado de la pantalla
    	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
     
     //Para definir el punto de vista
     glMatrixMode(GL_MODELVIEW);
     glLoadIdentity();
     
     gluLookAt(0.0, 0, 17,  // posicion del ojo
               0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0)
               0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)
     
     ////////////////////////////
     //		AQUI EMPIEZA MI DIBUJO
     ////////////////////////////
     char cad[100];
     sprintf(cad,"Jugador1: %d\n",puntos1);
     print(cad,10,0,1,1,1);
     sprintf(cad,"Jugador2: %d\n",puntos2);
     print(cad,650,0,1,1,1);
     int i;
     for(i=0;i<paredes.size();i++)
         paredes[i].Dibuja();
     
     fondo_izq.Dibuja();
     fondo_dcho.Dibuja();
     jugador1.Dibuja();
     jugador2.Dibuja();
     esfera.Dibuja();
     
     
     ////////////////////////////
     //		AQUI TERMINA MI DIBUJO
     ////////////////////////////
     
     //Al final, cambiar el buffer
     glutSwapBuffers();
 }
 
 void CMundoCliente::OnTimer(int value)
 {
     int i;
     int nLeido;

     char cad[MAX];
    
     //Actualizar campo de atributo de Memoria compartida
     pdatosBot->esfera=esfera;
     pdatosBot->raqueta1=jugador1;
     
     //invocar a OnKeyboardDown
     switch (pdatosBot->accion) {
         case 1:
             OnKeyboardDown('w', 0, 0);
             //jugador1.velocidad.y=4;
             break;
         case -1:
             //jugador1.velocidad.y=-4;
             OnKeyboardDown('s', 0, 0);
             break;
         case 0:
             jugador1.velocidad.y=0;
             break;
         default:
             break;
     }
     
    nLeido=read(fd_fifoCoord,cad,sizeof(cad));
    if(nLeido==-1){
        perror("Error al leer coordenadas(Estoy en MundoCliente)\n");
        exit(1);
    }
    else if(nLeido==0){
        perror("He llegado al final de la cadena de coordenadas(Estoy en MundoCliente)\n");
        exit(1);
    }
    else;
     
    //Actualizacion de informacion
    sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y,&esfera.radio, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);

     //Terminar si jugador 1 o 2 ha obtenido 3 puntos
     if(puntos1==3 ||puntos2==3)
    
        exit(1);
     
 }
 
 void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
 {

    char tecla[MAX];
    sprintf(tecla,"%c",key);
    write(fd_fifoTeclas,tecla,strlen(tecla)+1);

   
 }
 
 void CMundoCliente::Init()
 {

    int aux_error1, aux_error2;
    //Para inicializar esfera, ya que no calcula posicion
    esfera.centro.x=0;
    esfera.centro.y=0;
    
     Plano p;
     //pared inferior
     p.x1=-7;p.y1=-5;
     p.x2=7;p.y2=-5;
     paredes.push_back(p);
     
     //superior
     p.x1=-7;p.y1=5;
     p.x2=7;p.y2=5;
     paredes.push_back(p);
     
     fondo_izq.r=0;
     fondo_izq.x1=-7;fondo_izq.y1=-5;
     fondo_izq.x2=-7;fondo_izq.y2=5;
     
     fondo_dcho.r=0;
     fondo_dcho.x1=7;fondo_dcho.y1=-5;
     fondo_dcho.x2=7;fondo_dcho.y2=5;
     
     //a la izq
     jugador1.g=0;
     jugador1.x1=-6;jugador1.y1=-1;
     jugador1.x2=-6;jugador1.y2=1;
     
     //a la dcha
     jugador2.g=0;
     jugador2.x1=6;jugador2.y1=-1;
     jugador2.x2=6;jugador2.y2=1;
     

    //                      MEMORIA COMPARTIDA                //
     //Creacion de fichero para la memoria compartida
    fd_mem=open("/tmp/MemCompartida",O_RDWR|O_CREAT|O_TRUNC,0666);
     if(fd_mem==-1){
         printf("Error al abrir fichero para la memoria compartida(Estoy en MundoCliente)\n");
         close(fd_mem);
         exit(1);
     }
     //Definir el tamaño del fichero
     write(fd_mem,&datosBot,sizeof(datosBot));
     
     
     //Proyección de memoria
     //NULL: escribir donde quiera, tamaño de datosCompartidos,Proteccion lectura y escritura y region compartida.
    proyeccion=(char *)mmap(NULL,sizeof(datosBot),PROT_WRITE|PROT_READ,MAP_SHARED,fd_mem,0);//Devuelve la direccion de proyeccion utlizada.
    if (proyeccion==MAP_FAILED){
         perror("No puede abrir el fichero de proyeccion(Estoy en MundoCliente)\n");
         exit(1);
     }
     close(fd_mem);
    pdatosBot=(DatosMemCompartida*)proyeccion;
    
     pdatosBot->accion=0;
     
     //                      CREAR TUBERIA PARA COORDENADAS                 //
     unlink("/tmp/FifoCoordenadas");
    
    //Creacion de tuberia para RECIBIR coordenadas a cliente
    aux_error2=mkfifo("/tmp/FifoCoordenadas", 0666);
    if(aux_error2==-1){
        printf("error al crear fifo entre conecta Servidor y Cliente(Estoy en MundoCliente)\n");
        exit(1);
    }
    else
        printf("he creado correctamente el fifo de coordenadas(Estoy en MundoCliente)\n");
    
    //                      CREAR TUBERIA PARA TECLAS                 //
    unlink("/tmp/FifoTecla");//Para eliminar si ya existe
    //Creacion de tuberia para mandar teclas
    aux_error1=mkfifo("/tmp/FifoTecla", 0666);
    
    if(aux_error1==-1){
        printf("error al crear fifo entre conecta cliente y servidor(Estoy en MundoCliente)\n");
        exit(1);
    }
    else
        printf("he creado correctamente el fifo de teclas(Estoy en MundoCliente)\n");
    //                      ABRIR TUBERIA PARA COORDENADAS                 //
    fd_fifoCoord=open("/tmp/FifoCoordenadas", O_RDONLY);
    if(fd_fifoCoord==-1){
        perror("error al abrir fifo que recibe las coordenadas(Estoy en MundoCliente)\n");
        exit(1);
    }
    else
        printf("he abierto correctamente el fifo de coordenadas(Estoy en MundoCliente)\n");
    //                      ABRIR TUBERIA PARA TECLAS                 //
    fd_fifoTeclas=open("/tmp/FifoTecla",O_WRONLY);
    printf("El valor de fd_fifoTeclas es %d\n",fd_fifoTeclas);
    
    if(fd_fifoTeclas==-1){
        perror("error al abrir fifo que envia las teclas(Estoy en MundoCliente)\n");
        exit(1);
    }
    else
        printf("he abierto correctamente el fifo de teclas(Estoy en MundoCliente)\n");
 }
