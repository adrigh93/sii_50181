#include <iostream>
 #include <sys/types.h>
 #include <sys/stat.h>
 #include <sys/mman.h>
 #include <fcntl.h>
 #include <unistd.h>
 #include <stdio.h>
 #include <stdlib.h>
 #include "DatosMemCompartida.h"
 #include "Esfera.h"
 using namespace std;
 
 int main(){
     DatosMemCompartida *pdatosBot;
     int fd;
     char* proyeccion;
     bool seguir=1;
     //Abrir fichcero
     fd=open("/tmp/MemCompartida",O_RDWR);
     if(fd==-1){
         perror("Error al abrir fichero para la memoria compartida\n");
         close(fd);
         exit(1);
     }
     //Proyeccion de memoria
     pdatosBot=(DatosMemCompartida*)mmap(NULL,sizeof(*(pdatosBot)),PROT_WRITE|PROT_READ,MAP_SHARED,fd,0);
     if (pdatosBot==MAP_FAILED){
         perror("No puede abrir el fichero\n");
         exit(1);
     }
         
     close(fd);
     
     //Bucle infinito
     while(seguir==1){
         if(pdatosBot->accion==3)
             seguir=0;
         
         float centroRaqueta;
        centroRaqueta=((pdatosBot->raqueta1.y1 + pdatosBot->raqueta1.y2)/2);
         
         if(centroRaqueta < (pdatosBot->esfera.centro.y))
             (pdatosBot->accion)=1;
         
         else if(centroRaqueta > (pdatosBot->esfera.centro.y))
             (pdatosBot->accion)=-1;
         
         else
             pdatosBot->accion=0;
         
         
         usleep(2500);
     }
     munmap(proyeccion,sizeof(*(pdatosBot)));
     return 1;
 }