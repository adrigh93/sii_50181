#include <sys/types.h>
 #include <sys/stat.h>
 #include <stdio.h>
 #include <stdlib.h>
 #include <unistd.h>
 #include <sys/file.h>
 

#define MAX 100
 //Debe recibir el nombre y el numero de puntos que lleva el jugador
 int main(int argc, char *argv[]){
     int fd;
     char buffer[MAX];
     int aux_error;
     int nLeido;
     //crear tuberia

    unlink("/tmp/FifoPuntuacion");
     aux_error=mkfifo("/tmp/FifoPuntuacion",0777);
     if(aux_error==-1){
         printf("error al crear fifo de Logger\n");
         exit(1);
     }
    else
        printf("He creado correctamente el fifo de lOGGER (Estoy en Logger)\n");
     
     fd=open("/tmp/FifoPuntuacion",O_RDONLY);
     if(fd==-1){
         perror("error al abrir fifo de Logger\n");
         exit(1);
     }
    else
         printf("He abierto correctamente el fifo de lOGGER (Estoy en Logger)\n");
        
     printf("Puntuación inicial: Jugador1: 0 puntos, jugador 2: 0 puntos\n");
     
     
     //si lo ha creado correctamente, abrirlo en modo lectura
     while(1){
         nLeido=read(fd,buffer,sizeof(buffer));
         if(nLeido == -1){
            perror("Error lectura de fifo de Logger (Estoy en Logger)\n");
             exit(1);
         }
         else if(nLeido==0){

            perror("Ha llegado al final de cadena de Logger (Estoy en Logger)\n");
            close(fd);
            unlink("/tmp/FifoPuntuacion");
             exit(1);
         }
         else if(buffer[0]=='-'){
             printf("%s \n",buffer);
             break;
         }
         
         else
             printf("%s \n",buffer);
     }
     
     close(fd);
     unlink("/tmp/FifoPuntuacion");
     return 0;
 }
